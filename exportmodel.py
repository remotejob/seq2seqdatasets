from transformers import AutoModelWithLMHead, AutoTokenizer
import os
# directory = "output/content/models/tweetGPTfi/weights/checkpoint-140000"
directory = "models/best_model"
model = AutoModelWithLMHead.from_pretrained(directory)
tokenizer = AutoTokenizer.from_pretrained(directory)
out = "bert2bertv4_v2"
os.makedirs(out, exist_ok=True)
model.save_pretrained(out)
tokenizer.save_pretrained(out)