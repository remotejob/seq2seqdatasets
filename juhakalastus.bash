conda activate fidistillations

export KAGGLE_CONFIG_DIR=/home/juno/kagglejuhakalastus

python datautils.py
awk 'NR%100==0' data/pdaskans.csv > data/pdaskans_eval.csv
#export LIBTORCH=$HOME/repos/pytorch/libtorch
#export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
unset LIBTORCH
unset LD_LIBRARY_PATH

python savedataset.py

cp kdatasets/dataset-metadata.juhakalastus.json bigdatasets/dataset-metadata.json
kaggle datasets create -r zip -p bigdatasets/ 
kaggle datasets version -r zip -p bigdatasets/ -m "Updated data 0"



#DOWNLOAD
rm -rf lastoutput && mv output lastoutput && mkdir output
kaggle kernels output juhakalastus/krundistil1 -p output/

rm -rf output/models/best_model && rm -rf output/models/runs

cp kdatasets/dataset-metadata.juhakalastus.ckpoint.json output/models/dataset-metadata.json && kaggle datasets version -r zip -p output/models -m "Updated"
#kaggle datasets create -r zip -p output/models
python tstpipe.py 



#START
cp kernelgpu/kernel-metadata.juhakalastus.json kagglerun/kernel-metadata.json && kaggle kernels push -p kagglerun/