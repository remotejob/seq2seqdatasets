from transformers import BertTokenizerFast,AutoTokenizer
from datasets import Dataset
from torch.utils.data import IterableDataset

import pandas as pd

batch_size = 112  # 256/275/224 for finns 100 Roberta 128 multiling 186 #27 #64
encoder_max_length = 63
decoder_max_length = 31
pretrainedmodel = 'distilroberta-base'
# pretrainedmodel = 'distilbert-base-uncased'

#pretrainedmodel = 'remotejob/bert2bertv4_v1'

# tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel)
tokenizer = AutoTokenizer.from_pretrained(pretrainedmodel)
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token


# class CustomIterableDataset(IterableDataset):

#     def __init__(self, filename):

#         #Store the filename in object's memory
#         self.filename = filename

#         #And that's it, we no longer need to store the contents in the memory

#     def __iter__(self):distilroberta-base

#         #Create an iteratordistilroberta-base

def process_data_to_model_inputs(batch):
    # tokenize the inputs and labels

    inputs = tokenizer(batch["ask"], padding="max_length",
                       truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",
                        truncation=True, max_length=decoder_max_length)
    batch["attention_mask"] = inputs.attention_mask
    batch["decoder_input_ids"] = outputs.input_ids
    batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because RoBERTa automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]
    return batch

# df = pd.read_csv("data/pdaskans.csv", names=["ask", "answer"])[:3000000]
# # df = pd.read_csv("data/pdaskans.csv", chunksize=1000)

# # df = df[["ask", "answer"]]
# # df = pd.read_csv("/home/juno/common/data/twitterdialogs/pdaskans.csv", names=["ask", "answer"])
# # train_data = Dataset.from_pandas(df)
# # train_data = CustomIterableDataset()
# train_data = Dataset.from_pandas(df)


# train_data = train_data.map(
#     process_data_to_model_inputs,
#     batched=True,
#     batch_size=batch_size,
#     num_proc = 4,
#     keep_in_memory = True,
#     remove_columns=["ask", "answer"]
# )
# train_data.set_format(
#     type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
# )

# train_data.save_to_disk("datasets/train")

df = pd.read_csv("data/pdaskans_eval.csv", names=["ask", "answer"])[:30000]
# df = pd.read_csv("/home/juno/common/data/twitterdialogs/pdaskans_eval.csv", names=["ask", "answer"])

train_data = Dataset.from_pandas(df)


train_data = train_data.map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    num_proc = 4,
    keep_in_memory = True,
    remove_columns=["ask", "answer"]
)
train_data.set_format(
    type="torch", columns=["input_ids", "attention_mask", "decoder_input_ids", "decoder_attention_mask", "labels"],
)

train_data.save_to_disk("datasets/eval")






